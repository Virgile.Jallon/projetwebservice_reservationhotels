-- Lancer le script :       sqlite3 hotelReservations.db ".read /.../creationScript.sql"
-- Acceder au bash SQLite : sqlite3 hotelReservations.db

DROP TABLE IF EXISTS hotel;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS reservation;

CREATE TABLE hotel(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(255) NOT NULL
);


CREATE TABLE room(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    id_hotel INTEGER NOT NULL,
    name VARCHAR(255) NOT NULL,
    price FLOAT NOT NULL,
    informations VARCHAR(1000),
    FOREIGN KEY(id_hotel) REFERENCES hotel(id)
);


CREATE TABLE client(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL
);


CREATE TABLE reservation(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    id_client INTEGER NOT NULL,
    id_room INTEGER NOT NULL,
    check_in DATE NOT NULL,
    check_out DATE NOT NULL,
    FOREIGN KEY(id_client) REFERENCES client(id),
    FOREIGN KEY(id_room) REFERENCES room(id)
);



INSERT INTO hotel(name) VALUES 
("Les flots bleus       "),
("Hôtel de la plage     "),
("Le moulin de l'eclis  "),
("Le chalet             "),
("Le dortoire du routard");

INSERT INTO room(id_hotel, name, price, informations) VALUES
    -- Les flots bleus
    (1, "Chambre Crevette            ", 45, "Chambre classique"),
    (1, "Chambre Palourde            ", 55, "Chambre confort  "),
    (1, "Chambre Langoustine         ", 45, "Chambre classique"),
    (1, "Chambre Ecrevisse           ", 45, "Chambre confort  "),
    (1, "Chambre Ecume               ", 48, "Chambre modeste  "),
    -- Hôtel de la plage
    (2, "Chambre Soleil              ", 75, "Chambre de luxe  "),
    (2, "Chambre Cerf-volant         ", 65, "Chambre confort  "),
    (2, "Chambre Plage               ", 55, "Chambre confort  "),
    (2, "Chambre Touriste            ", 45, "Chambre classique"),
    (2, "Chambre Coquillage          ", 50, "Chambre classique"),
    -- Le moulin de l'eclis
    (3, "Chambre Moulin              ", 37, "Chambre classique"),
    (3, "Chambre Moisson             ", 42, "Chambre classique"),
    (3, "Chambre Avoine              ", 40, "Chambre classique"),
    -- Le chalet
    (4, "Chambre 101                 ", 40, "Chambre classique"),
    (4, "Chambre 102                 ", 40, "Chambre classique"),
    (4, "Chambre 103                 ", 40, "Chambre classique"),
    -- Le dortoire du routard
    (5, "Chambre DeGaulle            ", 60, "Chambre de luxe  "),
    (5, "Chambre MarieAntoinette     ", 80, "Chambre de luxe  "),
    (5, "Chambre JacquesBrel         ", 70, "Chambre de luxe  ")
;

INSERT INTO client(first_name, last_name, email, password) VALUES
    ("Virgile", "Jallon", "jallon.virgile@gmail.com", "936a185caaa266bb9cbe981e9e05cb78cd732b0b3280eb944412bb6f8f8f07af"), -- pwd = helloworld
    ("Laurent", "Lopes", "laurent.lopes@gmail.com", "936a185caaa266bb9cbe981e9e05cb78cd732b0b3280eb944412bb6f8f8f07af"),   -- pwd = helloworld
    ("Benjamin", "Bonnefoy", "benji.b@gmail.com", "4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2")      -- pwd = root
;

-- NOTE : Check_in = date de la 1ere nuit, check_out = date de départ
-- Ex : check_in le 10 et check_out le 13 => dort les nuits du 10,11,12 (soit 13-10 nuits)
INSERT INTO reservation (id_client, id_room, check_in, check_out) VALUES 
    (1, 2, "2020-10-10", "2020-10-13"),  -- Virgile - Chambre Palourde
    (1, 8, "2020-10-16", "2020-10-18"),  -- Virgile - Chambre Plage
    (1, 16, "2020-11-15", "2020-11-25"), -- Virgile - Chambre 103
    (1, 19, "2020-12-07", "2020-12-09"), -- Virgile - Chambre JacquesBrel

    (2, 5, "2020-10-02", "2020-10-05"),  -- Laurent - Chambre Ecume
    (2, 6, "2020-10-10", "2020-10-18"),  -- Laurent - Chambre Soleil
    (2, 12, "2020-11-10", "2020-11-15"), -- Laurent - Chambre Moisson 
    (2, 18, "2020-12-03", "2020-12-05"), -- Laurent - Chambre MarieAntoinette 

    (3, 1, "2020-09-20", "2020-10-20"), -- Benjamin - Chambre Crevette 
    (3, 3, "2020-11-01", "2020-11-20"), -- Benjamin - Chambre Langoustine 
    (3, 6, "2020-12-01", "2020-12-25")  -- Benjamin - Chambre Soleil 
;
