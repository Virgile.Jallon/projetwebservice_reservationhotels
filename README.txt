## Virgile Jallon

# Projet de Web-Service

L'objectif de ce projet est de créer un service web qui permet, depuis un client, de réserver des nuits à l'hôtel.

# Sommaire
  - Technologies utilisées
  - Structure du projet
  - Implémenter le projet sur sa machine
  - Démarrer le client
  - Utiliser le client


# Technologies utilisées :
Ce projet a été développé sur une machine Ubuntu 20.04. Il a été réalisé depuis l'IDE Eclipse JEE 2020-09.

Voici les différentes technologies utilisées :
Java : JavaSE-1.8 ET java-11-openjdk-amd64
Apache Axis2
Apache Tomcat v7.0
SQLite version 3.32.3
org.restlet.jar
org.json-chargebee-1.0.jar
sqlite-jdbc-3.32.3.2.jar

Un détail sera fourni plus bas sous forme de tableau récapitulatif


# Structure du projet

Ce projet s'articule autour de 4 projets :
	Client           - Client Java
	HotelReservation - SOAP
	HotelFiltering   - REST
	Servers          - TOMCAT


Notes :
- HotelReservation utilise le port 8081
- HotelFiltering utilise le port 8083
- On peut retrouver les services du SOAP à l'adresse http://localhost:8081/A_HotelReservation/services/listServices

Chacun des 4 projets utilise des librairies. Voici donc le tableau récapitulatif des librairies associées à chaque projet :

+-------------------------------------------------------+
| Librairies					| (A) | (B) | (C) | (D) |		Légende
|-------------------------------------------------------|       -------
| JavaSE-1.8					|     |     |  X  |     |       (A) : Client
|-------------------------------------------------------|       (B) : HotelReservation
| java-11-openjdk-amd64			|  X  |  X  |     |		|       (C) : HotelFiltering         
|-------------------------------------------------------|       (D) : Servers
| Apache Axis2					|     |  X  |     |     |        X  : Nécessite d'importer la librairie ou l'utilise
|-------------------------------------------------------|
| Apache Tomcat v7.0			|  X  |  X  |     |  X  |
|-------------------------------------------------------|
| org.restlet.jar               |     |  X  |  X  |     |
|-------------------------------------------------------|
| org.json-chargebee-1.0.jar    |  X  |  X  |  X  |     |
|-------------------------------------------------------|
| sqlite-jdbc-3.32.3.2.jar      |     |     |  X  |     |
+-------------------------------------------------------+



# Implémenter le projet sur sa machine

Implémenter le projet sur sa machine nécessite d'effectuer 4 étapes :

1 - S'assurer d'avoir installé les technologies nécessaires dans les bonnes versions.

2 - Importer les projets sur sa machine depuis Eclipse.
Normalement les librairies nécessaires se trouvent déjà dans les dossiers.
Si malgré tout, les projets semblent comporter des erreurs, il est nécessaire de ré-importer les librairies.
Pour les imports, se rapporter au tableau ci-dessus

3 - Lancer le script de création de la base de donées
Pour le lancer depuis un environement Unix : 
sqlite3 hotelReservations.db ".read /leCheminMenantAuDossier/creationScript.sql"
Note : Si on souhaite accéder à la base de données, il faut utiliser la commande : sqlite3 hotelReservations.db

4 - Modifier le fichier HotelFiltering->src->hotelFiltering->DBManager.java
La ligne 25 est actuellement comme celà : _dbPath = "/home/plops/hotelReservations.db";
Il est nécessaire de changer le chemin de façon à ce que celui-ci pointe vers l'emplacement où a été créé le fichier hotelReservations.db après le lancement du script de création.
(Sous Unix, "/home/username/hotelReservations.db" par défaut)



# Démarrer le client

Pour pouvoir utiliser l'application, il faut effectuer les manipulations suivantes :

1 - Effectuer un clic droit sur HotelReservation/src/test/HotelReservation.java
	-> Web Services -> Create Web Service -> Cliquer sur Web service runtime puis sélectionner Apache Axis 2 -> Finish

2 - Clic droit sur HotelReservation -> Run As -> Run On Server -> Sélectionner Tomcat v7.0 -> Next -> Vérifier que HotelReservation est bien dans la colonne "configured" -> Finish

3 - Clic droit sur HotelFiltering->src-HotelFiltering->RESTDistributor.java -> Run As -> Java Application

4 - Clic droit sur Client->Java Resources->src->client->main.java -> Run As -> Java Application


# Utiliser le client

Le client est une application "console".
L'utilisateur peut naviguer dans les menus et effectuer des choix en saisissant le numéro associé à une proposition.
L'utilisateur est parfois invité à saisir du texte.

Par défaut, il existe plusieurs comptes, accessibles avec les identifiants suivants :

+---------------------------------------------------------------------+
|Prénom    |  Nom       |  Email                      |  Mot de passe |
+---------------------------------------------------------------------+
|Virgile   |  Jallon    |  jallon.virgile@gmail.com   |  helloworld   |
|Laurent   |  Lopes     |  laurent.lopes@gmail.com    |  helloworld   |
|Benjamin  |  Bonnefoy  |  benji.b@gmail.com          |  root         |
+---------------------------------------------------------------------+