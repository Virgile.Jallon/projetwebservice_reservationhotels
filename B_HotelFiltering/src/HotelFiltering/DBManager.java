package HotelFiltering;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.json.JSONArray;
import org.json.JSONObject;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Classe qui a pour objectif de manipuler la base de données
 */
public abstract class DBManager {
	
	/**
	 * Une connexion à la base de données
	 */
	private static Connection _connection;
	
	/**
	 * Le chemin vers la base de données
	 */
	private static String _dbPath = "/home/plops/hotelReservations.db";
	
	/**
	 * Constructeur par défaut
	 */
	public DBManager() {
		// Initialisation de la connexion à la base de données
		connect();
	}
	
	/**
	 * La connexion à la BDD est-elle possible ?
	 * Si la connexion à la BDD est nulle, initie une nouvelle connexion.
	 * @return True si la connexion à la BDD est possible. False sinon.
	 */
	public static boolean isDatabaseReacheable() {
		if(_connection != null) {
			return true;
		}
		return connect();
	}
	
	        
	/**
	 * Initie la connexion à la base de données
	 * @return true si la connexion a réussie. False sinon
	 */
    private static boolean connect() {
    	String connectionString = "jdbc:sqlite:"+_dbPath;
        try {
            _connection = DriverManager.getConnection(connectionString); 
        } catch (Exception e) {
        	_connection = null;
        }
        return _connection != null;
    }
 
    /**
     * Vérifie qu'une adresse email n'est pas encore présente dans la base de données
     * @param email L'email dont on souhaite vérifier l'absence en base de données
     * @return true si l'email n'est pas présente
     * <br>    false si l'email est présente
     * <br>    null si une exception a eu lieu
     */
    public static Boolean isEmailAvailable(String email) {
    	String query = "SELECT COUNT(*) AS total FROM client where email=?";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setString(1,email);
            ResultSet rs  = pstmt.executeQuery(); // Execution de la requête
            int total = rs.getInt("total");
        	rs.close();
        	pstmt.close();
        	
        	// Si aucune ligne ne contient l'adresse email recherchée, retourne true sinon false
            return total == 0;          
    	}catch(Exception e) {
    		return null;
    	}
    }
      
    /**
     * Applique le Hache Sha256 à un String
     * @param text Le texte à hacher
     * @return Le texte haché (String) ou null si le hachage a échoué
     * Note : Le code initial est issu d'internet mais a été modifié pour l'adapter aux besoins
     */
    public static String toSha256(String text) {
    	try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[]hashInBytes = md.digest(text.getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
    	}
    	catch (Exception e) {
			return null;
		}

    }
    
    
    /**
     * Vérifie que le couple (email , password) correspond bien à un compte client existant
     * @param email Une adresse email
     * @param password UN mot de passe
     * @return True si un compte correspondant existe
     *    <br> False si aucun compte correspondant n'existe
     *    <br> Null si une exception a eu lieu
     */
    public static Boolean areLoginInputsCorrect(String email, String password) {
    	String cryptedPassword = toSha256(password);
    	if(cryptedPassword == null) {
    		return null; // Impossible de crypter le mot de passe
    	}
    	
    	String query = "SELECT COUNT(*) AS total FROM client where email=? AND password=?";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setString(1,email);
            pstmt.setString(2,cryptedPassword);
            ResultSet rs  = pstmt.executeQuery();  // Execution de la requête
            int nbMatchingClients = rs.getInt("total");
        	rs.close();
        	pstmt.close();
        	// Retourne true si 1 et 1 seul client correspond au couple identifiant/mot de passe
            return nbMatchingClients == 1;          
    	}catch(Exception e) {
    		return null;
    	}
    }
    
    
    /**
     * Retourne l'ID du client associé au couple (email , password)
     * @param email L'email du client qui souhaite se connecter
     * @param cryptedPassword Le mot de passe (crypté) du client qui souhaite se connecter
     * @return L'id du client associé au couple (email , password).
     * <br>    -1 si une erreur s'est produite
     */
    public static int login(String email, String cryptedPassword) {
    	String query = "SELECT id FROM client where email=? AND password=?";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setString(1,email);
            pstmt.setString(2,cryptedPassword);
            ResultSet rs  = pstmt.executeQuery();  // Execution de la requête
            int id = rs.getInt("id");
        	rs.close();
        	pstmt.close();
        	// Retourne l'id du client si celui-ci est correct. Retourne -1 sinon
            return id >= 1 ? id : -1;
    	}
    	catch(Exception e){
    		return -1;
    	}
    }
    
    /**
     * Retourne des informations sur un client
     * @param clientId L'ID du client
     * @return Un JSONObject représentant des données relatives au client :
     * <br>id
     * <br>first_name
     * <br>last_name
     * <br>email
     * Retourne null si une exception a eu lieu
     */
    public static JSONObject getClientById(int clientId) {
    	String query = "SELECT * FROM client where id=?";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setInt(1,clientId);
            
            // Execution de la requête
            ResultSet rs  = pstmt.executeQuery();
            
            // Construction du JSONObject
        	JSONObject ret = new JSONObject();
        	ret.put("id", rs.getInt("id"));
        	ret.put("first_name", rs.getString("first_name"));
        	ret.put("last_name", rs.getString("last_name"));
        	ret.put("email", rs.getString("email"));
        	
        	// On ferme proprement le ResultSet puis le PreparedStatement
        	rs.close();
        	pstmt.close();
        	
        	return ret;
    	}
    	catch(Exception e){
    		return null;
    	}
    }
    
    /**
     * Supprime une réservation de la BDD
     * @param reservationId l'ID de la réservation à supprimer
     * @return False si une erreur est survenue lors de la suppression. True sinon.
     */
    public static boolean undoReservation(int reservationId) {
    	String query = "DELETE FROM reservation where id = ?";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setInt(1,reservationId);
            
            // Execution de la requête
            pstmt.executeUpdate();  
            
            // On ferme proprement le PreparedStatement
        	pstmt.close();
        	
        	return true;
    	}
    	catch(Exception e){
    		return false;
    	}
    }
    

    /**
     * Ajoute une nouvelle réservation en BDD
     * @param idClient L'ID du client à l'origine de la réservation
     * @param idRoom L'ID de la chambre à réserver
     * @param checkIn La date de début de réservation (format string yyyy-mm-dd)
     * @param checkOut La date de départ (format string yyyy-mm-dd)
     * @return True si l'insertion s'est bien déroulée. False sinon.
     */
    public static boolean makeAReservation(int idClient, int idRoom, String checkIn, String checkOut) {
    	String query = "INSERT INTO reservation (id_client, id_room, check_in, check_out) VALUES (?, ?, ?, ?)";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setInt(1,idClient);
            pstmt.setInt(2,idRoom);
            pstmt.setString(3, checkIn);
            pstmt.setString(4, checkOut);
            
            // Execution de la requête
            pstmt.executeUpdate();  
            
            // On ferme proprement le PreparedStatement
        	pstmt.close();
        	return true;
    	}
    	catch(Exception e){
    		return false;
    	}
    }
    
    /**
     * Crée un compte client en BDD
     * @param firstName Prénom du client
     * @param lastName Nom de famille du client
     * @param email Email du client
     * @param cryptedPassword Le mot de passe (crypté) du client
     * @return True si l'insertion s'est bien déroulée. False sinon.
     */
    public static boolean createClient(String firstName, String lastName, String email, String cryptedPassword) {
    	String query = "INSERT INTO client (first_name,last_name,email,password) VALUES (?,?,?,?)";
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
            pstmt.setString(1, firstName);
            pstmt.setString(2, lastName);
            pstmt.setString(3, email);
            pstmt.setString(4, cryptedPassword);
            
            // Execution de la requête
            pstmt.executeUpdate();  
            
            // On ferme proprement le PreparedStatement
        	pstmt.close();
        	return true;
    	}
    	catch(Exception e){
    		return false;
    	}
    }
    
    // TODO : commenter
    /**
     * Renvoie des informations concernant les chambres disponibles sur la période spécifiée 
     * @param arrivalDate Date d'arrivée (au format String)
     * @param departureDate Date de départ (au format String)
     * @param nbRooms Nombre de chambres
     * @return Un JSONArray composé de JSONObject représentant des données relatives aux chambres disponibles :
     * <br> roomId
     * <br> hotelId
     * <br> roomName
     * <br> hotelName
     * <br> price
     * <br> informations
     * Retourne null si une exception a eu lieu
     */
    public static JSONArray getAvailableRooms(String arrivalDate, String departureDate, int nbRooms) {
		String query = "SELECT DISTINCT hotel.id as hotelId, hotel.name as hotelName, room.id as roomId, room.name as roomName, price, informations FROM room, hotel WHERE\n"
				+ "room.id NOT IN(\n"
				+ "    SELECT id_room FROM reservation WHERE\n"
				+ "    (? BETWEEN check_in AND check_out)\n"
				+ "    OR\n"
				+ "    (? BETWEEN check_in AND check_out)\n"
				+ ")\n"
				+ "AND hotel.id == room.id_hotel;\n"
				+ "";
    	
    	try {   		
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
    		pstmt.setString(1, arrivalDate);
    		pstmt.setString(2, departureDate);
    		
    		// Execution de la requête
            ResultSet rs  = pstmt.executeQuery();
            
            // Création du JSONArray
            JSONArray ret = new JSONArray();
            while (rs.next()) {
            	// Création d'un JSONObject temporaire
            	JSONObject tempJsonObj = new JSONObject();
            	tempJsonObj.put("roomId", rs.getInt("roomId"));
            	tempJsonObj.put("hotelId", rs.getInt("hotelId"));
            	tempJsonObj.put("roomName", rs.getString("roomName"));
            	tempJsonObj.put("hotelName", rs.getString("hotelName"));
            	tempJsonObj.put("price", rs.getString("price"));
            	tempJsonObj.put("informations", rs.getString("informations"));
            	
            	// Ajout du JSONObject dans le JSONArray
            	ret.put(tempJsonObj);
            }
            
            // On ferme proprement le ResultSet puis le PreparedStatement
        	rs.close();
        	pstmt.close();
        	
        	return ret;
    	}
    	catch(Exception e){
    		return null;
    	}
    }
    
    
    
    /**
     * Récupère les réservations qui rentrent en conflit avec une nouvelle réservation
     * @param clientId L'identifiant du client
     * @param arrivalDate La date d'arrivée (au format String)
     * @param departureDate La date de départ (au format String)
     * @return Un JSONArray composé de JSONObject représentant des données relatives aux réservations conflictuelles :
     * <br> id
     * <br> hotelName
     * <br> roomName
     * <br> hotelName
     * <br> price
     * <br> informations
     * <br> check_in
     * <br> check_out
     * Retourne null si une exception a eu lieu
     */
    public static JSONArray getConstrainedReservations(int clientId, String arrivalDate, String departureDate) {
    	String query = "SELECT reservation.id as id ,hotel.name as hotelName, room.name as roomName, price, informations, check_in, check_out\n"
				+ "FROM reservation\n"
				+ "INNER JOIN room on room.id = reservation.id_room\n"
				+ "INNER JOIN hotel on hotel.id = room.id_hotel\n"
				+ "WHERE (\n"
				+ "(? BETWEEN check_in AND check_out)\n"
				+ "OR\n"
				+ "(? BETWEEN check_in AND check_out))\n"
				+ "AND id_client = ?";
    	
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
    		pstmt.setString(1, arrivalDate);
    		pstmt.setString(2, departureDate);
    		pstmt.setInt(3,clientId);
    		
    		// Execution de la requête
            ResultSet rs  = pstmt.executeQuery(); 
            
            // Création du JSONArray
            JSONArray ret = new JSONArray();
            while (rs.next()) {
            	// Création d'un JSONObject temporaire
            	JSONObject tempJsonObj = new JSONObject();
            	tempJsonObj.put("id", rs.getInt("id"));
            	tempJsonObj.put("hotelName", rs.getString("hotelName"));
            	tempJsonObj.put("roomName", rs.getString("roomName"));
            	tempJsonObj.put("price", String.valueOf(rs.getFloat("price")));
            	tempJsonObj.put("informations", rs.getString("informations"));
            	tempJsonObj.put("check_in", rs.getString("check_in"));
            	tempJsonObj.put("check_out", rs.getString("check_out"));
            	
            	// Ajout du JSONObject dans le JSONArray
            	ret.put(tempJsonObj);
            }
            
            // On ferme proprement le ResultSet puis le PreparedStatement
        	rs.close();
        	pstmt.close();
        	
        	return ret;
    	}
    	catch(Exception e){
    		return null; 
    	}
    }
    
    
    /**
     * Récupère toutes les réservations effectuées par un client
     * @param clientId L'id du client dont on souhaite connaître les réservations
     * @return Un JSONArray contenant des JSONObject. Chaque JSONObjet décrit une réservation et contient les champs suivantes : 
     * <br> id
     * <br> hotelName
     * <br> roomName
     * <br> hotelName
     * <br> price
     * <br> informations
     * <br> check_in
     * <br> check_out
     * Retourne null si une erreur s'est produite.
     */
    public static JSONArray getClientReservations(int clientId) {
		String query = "select reservation.id as id ,hotel.name as hotelName, room.name as roomName, price, informations, check_in, check_out from reservation\n"
				+ "inner join room on room.id = reservation.id_room\n"
				+ "inner join hotel on hotel.id = room.id_hotel\n"
				+ "where id_client = ?\n"
				+ "ORDER by reservation.check_out";
		
    	try {
    		// Préparation de la requête
    		PreparedStatement pstmt  = _connection.prepareStatement(query);
    		pstmt.setInt(1,clientId);
    		
    		// Execution de la requête
            ResultSet rs  = pstmt.executeQuery();  
            
            // Création d'un JSONObject temporaire
            JSONArray ret = new JSONArray();
            while (rs.next()) {
            	JSONObject tempJsonObj = new JSONObject();
            	tempJsonObj.put("id", rs.getInt("id"));
            	tempJsonObj.put("hotelName", rs.getString("hotelName"));
            	tempJsonObj.put("roomName", rs.getString("roomName"));
            	tempJsonObj.put("price", String.valueOf(rs.getFloat("price")));
            	tempJsonObj.put("informations", rs.getString("informations"));
            	tempJsonObj.put("check_in", rs.getString("check_in"));
            	tempJsonObj.put("check_out", rs.getString("check_out"));
            	
            	// Ajout du JSONObject dans le JSONArray
            	ret.put(tempJsonObj);
            }
            
            // On ferme proprement le ResultSet puis le PreparedStatement
        	rs.close();
        	pstmt.close();
        	
        	return ret;
    	}
    	catch(Exception e){
    		return null;
    	}
    }

    
}