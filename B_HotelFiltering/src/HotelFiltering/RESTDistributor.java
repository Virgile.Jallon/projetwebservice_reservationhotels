package HotelFiltering;
 
import org.restlet.Component;
import org.restlet.data.Protocol;

/**
 * Crée un nouveau composant Restlet
 */
public class RESTDistributor {
 
	public static void main(String[] args) throws Exception {
		// Crée un nouveau compsosant Restlet et y ajoute un serveur HTTP
		Component component = new Component();  
		component.getServers().add(Protocol.HTTP, 8083); 
		component.getDefaultHost().attach(new RouterApplication());  
		component.start();
 

	}	 
 
}