package HotelFiltering;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONArray;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
 
/**
 * Cherche la liste des chambres disponibles sur la période recherchée
 */
public class Search extends ServerResource {  
	
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
		String arrivalDate = form.getFirstValue("arrivalDate");
		int nbNights = Integer.parseInt(form.getFirstValue("nbNights"));
		int nbRooms = Integer.parseInt(form.getFirstValue("nbRooms"));

		// Création d'une date égale à arrivalDate + nBNights
		// Ex : arrivalDate = 2020-10-10 nbNights = 3 ==> departureDate = 2020-10-13
    	String departureDate = "";
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar c = Calendar.getInstance();
    	try{
    		c.setTime(sdf.parse(arrivalDate));
    	}
    	catch(Exception e){
    		return null;
    	}
       	c.add(Calendar.DAY_OF_MONTH, nbNights-1);
       	departureDate = sdf.format(c.getTime());

       	// Recherche des chambres disponibles
		JSONArray resultatsRecherche = DBManager.getAvailableRooms(arrivalDate, departureDate, nbRooms);
		if(resultatsRecherche == null) {
			return null;
		}
		
		return new StringRepresentation(resultatsRecherche.toString());
    } 
    

}  