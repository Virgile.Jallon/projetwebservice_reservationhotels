package HotelFiltering;
 
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Annule une réservation
 */
public class Unbook extends ServerResource {  
	
    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
		int reservationID = Integer.parseInt(form.getFirstValue("reservationID"));
		boolean state = DBManager.undoReservation(reservationID);
		return new StringRepresentation(String.valueOf(state));
    } 

    
}  