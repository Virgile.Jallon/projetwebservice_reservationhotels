package HotelFiltering;
 
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Vérifie que le REST el la base de données peuvent communiquer
 */
public class CheckDatabaseAvailability extends ServerResource {  
	
    @Post
    public Representation acceptItem(Representation entity) { 
    	return new StringRepresentation(String.valueOf(DBManager.isDatabaseReacheable()));
    } 
    
    
}  