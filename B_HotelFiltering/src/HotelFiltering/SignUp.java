package HotelFiltering;
 
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Crée un compte pour un nouveau client
 */
public class SignUp extends ServerResource {  
    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
		String firstName = form.getFirstValue("firstName");
		String lastName = form.getFirstValue("lastName");
		String email = form.getFirstValue("email");
		String password = form.getFirstValue("password");

		Boolean isEmailAvailable = DBManager.isEmailAvailable(email);
		// Si aucun compte n'a déjà été créé avec cette adresse email
		if(isEmailAvailable) {
			String cryptedPassword = DBManager.toSha256(password);
			// Création du compte client en BDD
			if(DBManager.createClient(firstName, lastName, email, cryptedPassword)) {
				return new StringRepresentation("true");
			}
		}
		return new StringRepresentation("false");
    } 
    
}  