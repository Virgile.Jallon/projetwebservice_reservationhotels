package HotelFiltering;
 
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONArray;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Récupère les réservations conflictuelles
 */
public class GetConstrainedReservations extends ServerResource {  
	
    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
    	String arrivalDate = form.getFirstValue("arrivalDate");
		int nbNights = Integer.parseInt(form.getFirstValue("nbNights"));
		int clientId = Integer.parseInt(form.getFirstValue("clientId"));

		// Création d'une date égale à arrivalDate + nBNights
		// Ex : arrivalDate = 2020-10-10 nbNights = 3 ==> departureDate = 2020-10-13
    	String departureDate = "";
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar c = Calendar.getInstance();
    	try{
    		c.setTime(sdf.parse(arrivalDate));
    	}
    	catch(Exception e){
    		return null;
    	}
       	c.add(Calendar.DAY_OF_MONTH, nbNights-1);
       	departureDate = sdf.format(c.getTime());

       	// Recherche des chambres contraignantes
    	JSONArray constrainedReservations = DBManager.getConstrainedReservations(clientId, arrivalDate, departureDate);
    	if(constrainedReservations == null) {
    		return null;
    	}
    	return new StringRepresentation(constrainedReservations.toString());
    } 
 
    
}  