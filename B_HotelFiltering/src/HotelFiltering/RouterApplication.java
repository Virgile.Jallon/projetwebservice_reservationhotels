package HotelFiltering;
 
import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

 
public class RouterApplication extends Application{
	/**
	 * Crée une racine Restlet qui va recevoir tous les appels entrants
	 */
	@Override
	public synchronized Restlet createInboundRoot() {
		// Crée un routeur Restlet qui va router chaque appel vers une instance de ressource
		Router router = new Router(getContext());
		router.attach("/search", 						HotelFiltering.Search.class);
		router.attach("/book", 							HotelFiltering.Book.class);
		router.attach("/checkdatabaseavailability", 	HotelFiltering.CheckDatabaseAvailability.class);
		router.attach("/getreservations", 				HotelFiltering.GetReservations.class);
		router.attach("/unbook", 						HotelFiltering.Unbook.class);
		router.attach("/getconstrainedreservations", 	HotelFiltering.GetConstrainedReservations.class);
		router.attach("/signin", 						HotelFiltering.SignIn.class);
		router.attach("/signup", 						HotelFiltering.SignUp.class);
		return router;
	}
}