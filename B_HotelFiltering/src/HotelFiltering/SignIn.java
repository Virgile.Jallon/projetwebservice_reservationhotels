package HotelFiltering;

import org.json.JSONObject;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Permet à un client de se connecter à son compte
 */
public class SignIn extends ServerResource {  

    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
		String email = form.getFirstValue("email");
		String password = form.getFirstValue("password");
		
		JSONObject jsonRet = new JSONObject();
		// Si la combinaison identifiant/mot de passe est valide
		if(DBManager.areLoginInputsCorrect(email, password)) {
			String cryptedPassword = DBManager.toSha256(password);
			// On connecte le client à son compte
			int clientId = DBManager.login(email, cryptedPassword);
			if(clientId != -1) {
				// On récupère des informations sur le client qui vient de se connecter
				jsonRet = DBManager.getClientById(clientId);
		        return new StringRepresentation(jsonRet.toString());
			}
		}
		return null;
		


    
    
    } 
    
}  