package HotelFiltering;
 
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Réserve une chambre
 */
public class Book extends ServerResource {  

    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity);
    	int clientID = Integer.parseInt(form.getFirstValue("clientID"));
    	int roomID = Integer.parseInt(form.getFirstValue("roomID"));
    	String arrivalDate = form.getFirstValue("arrivalDate");
    	int nbNights = Integer.parseInt(form.getFirstValue("nbNights"));
    	
    	// Création d'un calendrier
    	String checkOut = "";
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar c = Calendar.getInstance();
    
    	try{
    		// Initialisation du calendrier
    		c.setTime(sdf.parse(arrivalDate));
    	}
    	catch(Exception e){
    		return new StringRepresentation("");
    	}
    	
    	// Définition de la date de checkOut
       	c.add(Calendar.DAY_OF_MONTH, nbNights);  
       	checkOut = sdf.format(c.getTime());

       	// Insertion en BDD
       	boolean state = DBManager.makeAReservation(clientID, roomID, arrivalDate, checkOut);
    	return new StringRepresentation(String.valueOf(state));
    } 
    

    
}  