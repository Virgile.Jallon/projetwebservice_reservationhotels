package HotelFiltering;
 

import org.json.JSONArray;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
 * Récupère les réservations d'un client
 */
public class GetReservations extends ServerResource {  

    
    @Post
    public Representation acceptItem(Representation entity) { 
    	Form form = new Form(entity); 
    	int clientId = Integer.parseInt(form.getFirstValue("clientId"));
    	
    	JSONArray reservations = DBManager.getClientReservations(clientId);
    	if(reservations == null) {
    		return null;
    	}
    	return new StringRepresentation(reservations.toString());
    } 

    
}  