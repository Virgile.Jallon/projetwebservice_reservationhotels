package test;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.data.Form;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;

/*
 * Classe publique qui représente les différents services proposés par le SOAP
 */
public class HotelReservation {
	
	/**
	 * Le client qui fait appel au REST
	 */
	private Client _client = null;
	
	/**
	 * Appelle le REST et récupère la valeur retournée par celui-ci.
	 * @param route Complément d'URL nécessaire à l'appel du REST (détail dans la classe RouterApplication)
	 * @param parameters Paramètres sous forme d'un objet JSON
	 * @return La valeur retournée par le REST (au format String). "" si une erreur est survenue.
	 */
	@SuppressWarnings("rawtypes")
	private String callRest(String route, JSONObject parameters) {
		// Création du client si celui-ci n'a jamais été créé
		if(_client == null) {
			_client = new Client(new Context(), Protocol.HTTP);
		}
		ClientResource resource = new ClientResource("http://localhost:8083/"+route);  
	    resource.setNext(_client);	
		Form form = new Form();  
		try {
			// Passe au formulaire les informations contenues dans le JSON 
			Iterator keys = parameters.keys();
			while(keys.hasNext()) {
			    String key = (String) keys.next();
			    form.add(key, parameters.getString(key));
			}
			
			return resource.post(form).getText();
		}
		catch(Exception e) {
			return "";
		}
		
	}
	
	/**
	 * Retourne les hôtels disponibles pour les critères de recherche spécifiés
	 * @param arrivalDate Date d'arrivée (au format String)
	 * @param nbNights Nombre de nuits (au format String)
	 * @param nbRooms Nombre de chambres (au format String)
	 * @return Le détail des hôtels disponibles (JSONArray au format String). Null si une erreur est survenue
	 */
	public String checkHotelsAvailability(String arrivalDate, String nbNights, String nbRooms){
		JSONObject json = new JSONObject();
		try {
			// Ajout d'informations dans le JSON
			json.put("arrivalDate", arrivalDate);
			json.put("nbNights", nbNights);
			json.put("nbRooms", nbRooms);
			
			return callRest("search", json);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	/**
	 * Crée une nouvelle réservation
	 * @param clientID L'ID du client à l'origine de la réservation
	 * @param roomID L'ID de la chambre à réserver
	 * @param arrivalDate La date de début de la réservation (format string yyyy-mm-dd)
	 * @param nbNights La date de départ (format string yyyy-mm-dd)
	 * @return "true" si la réservation s'est bien passée
	 * 	   <br>"false" si la réservation a échouée
	 *     <br> "" si une erreur est survenue
	 */
	public String bookRoom(String clientID, String roomID, String arrivalDate, String nbNights){
		JSONObject json = new JSONObject();
		try {
			json.put("clientID", clientID);
			json.put("roomID", roomID);
			json.put("arrivalDate", arrivalDate);
			json.put("nbNights", nbNights);
			return callRest("book", json);
		}
		catch(JSONException e) {
			return "";
		}	
	}
	
	/**
	 * Vérifie que la BDD est bien atteignable
	 * @return True si la BDD est atteignable, False sinon.
	 */
	public boolean isDatabaseReacheable() {
		String result = callRest("checkdatabaseavailability", new JSONObject());
		return Boolean.parseBoolean(result);
	}
	
	/**
	 * Retourne un JSONArray sous la forme d'un String. Celui-ci représente toutes les réservations
	 * ayant été effectuées par le client
	 * @param clientId L'id du client dont on souhaite connaître les réservations
	 * @return Un JSONArray sous la forme d'un String. Retourne null si une exception a eu lieu
	 */
	public String getReservations(String clientId) {
		JSONObject json = new JSONObject();
		try {
			json.put("clientId", clientId);
			return callRest("getreservations", json);
		}
		catch(Exception e) {
			return null;
		}	
	}
	
	/**
	 * Annule une réservation
	 * @param reservationID l'ID de la réservation sous forme de String
	 * @return False si une erreur est survenue lors de la suppression. True sinon.
	 */
	public boolean unbookReservation(String reservationID) {
		try {
			JSONObject json = new JSONObject();
			json.put("reservationID", reservationID);
			return Boolean.parseBoolean(callRest("unbook", json));
		}
		catch(Exception e) {
			return false;
		}
	}
	
	/**
	 * Récupère les réservations qui rentrent en conflit avec la réservations spécifiée
	 * @param clientId L'identifiant du client (au format String)
	 * @param arrivalDate La date d'arrivée (au format String)
	 * @param nbNights Le nombre de nuits (au format String)
	 * @return Un JSONArray au format String qui contient des informations sur les réservations conflictuelles. "" Si une exception a eu lieu.
	 * 
	 * Exemple : Le client a déjà fait les réservations suivantes :  (1) 2020-01-10  -->  2020-01-15
	 * 													             (2) 2020-01-20  -->  2020-01-25
	 * Si il souhaite faire une nouvelle réservation 2020-01-23  -->  2020-01-27, la réservation (2) est conflictuelle
	 */
	public String getConstrainedReservations(String clientId, String arrivalDate, String nbNights) {
		JSONObject json = new JSONObject();
		try {
			json.put("clientId", clientId);
			json.put("arrivalDate", arrivalDate);
			json.put("nbNights", nbNights);
			return callRest("getconstrainedreservations", json);
		}
		catch(Exception e) {
			return "";
		}
	}
	

	/**
	 * Vérifie que l'email et le mot de passe fournis correspondent bien à un compte client existant
	 * Si c'est le cas, retourne des informations relatives au client
	 * @param email L'email du client qui souhaite se connecter
	 * @param password Le mot de passe du client qui souhaite se connecter
	 * @return Un String représentant un JSONObject. Si une erreur s'est produite, retourne null.
	 */
	public String signIn(String email, String password) {
		JSONObject json = new JSONObject();
		try {
			json.put("email", email);
			json.put("password", password);
			return callRest("signin", json);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	/**
	 * Crée un nouveau compte client
	 * @param firstName Prénom du client
	 * @param lastName Nom de famille du client
	 * @param email Email du client
	 * @param password Mot de passe du client
	 * @return True si la création du compte s'est bien déroulée. False sinon.
	 */
	public boolean signUp(String firstName, String lastName, String email, String password) {
		JSONObject json = new JSONObject();
		try {
			json.put("firstName", firstName);
			json.put("lastName", lastName);
			json.put("email", email);
			json.put("password", password);
			return Boolean.parseBoolean(callRest("signup", json));
		}
		catch(Exception e) {
			return false;
		}
	}
	
	
}