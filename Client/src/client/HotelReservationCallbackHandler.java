
/**
 * HotelReservationCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package client;

    /**
     *  HotelReservationCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class HotelReservationCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public HotelReservationCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public HotelReservationCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for checkHotelsAvailability method
            * override this method for handling normal response from checkHotelsAvailability operation
            */
           public void receiveResultcheckHotelsAvailability(
                    client.HotelReservationStub.CheckHotelsAvailabilityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from checkHotelsAvailability operation
           */
            public void receiveErrorcheckHotelsAvailability(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getReservations method
            * override this method for handling normal response from getReservations operation
            */
           public void receiveResultgetReservations(
                    client.HotelReservationStub.GetReservationsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getReservations operation
           */
            public void receiveErrorgetReservations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for bookRoom method
            * override this method for handling normal response from bookRoom operation
            */
           public void receiveResultbookRoom(
                    client.HotelReservationStub.BookRoomResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from bookRoom operation
           */
            public void receiveErrorbookRoom(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isDatabaseReacheable method
            * override this method for handling normal response from isDatabaseReacheable operation
            */
           public void receiveResultisDatabaseReacheable(
                    client.HotelReservationStub.IsDatabaseReacheableResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isDatabaseReacheable operation
           */
            public void receiveErrorisDatabaseReacheable(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getConstrainedReservations method
            * override this method for handling normal response from getConstrainedReservations operation
            */
           public void receiveResultgetConstrainedReservations(
                    client.HotelReservationStub.GetConstrainedReservationsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getConstrainedReservations operation
           */
            public void receiveErrorgetConstrainedReservations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for unbookReservation method
            * override this method for handling normal response from unbookReservation operation
            */
           public void receiveResultunbookReservation(
                    client.HotelReservationStub.UnbookReservationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from unbookReservation operation
           */
            public void receiveErrorunbookReservation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for signUp method
            * override this method for handling normal response from signUp operation
            */
           public void receiveResultsignUp(
                    client.HotelReservationStub.SignUpResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from signUp operation
           */
            public void receiveErrorsignUp(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for signIn method
            * override this method for handling normal response from signIn operation
            */
           public void receiveResultsignIn(
                    client.HotelReservationStub.SignInResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from signIn operation
           */
            public void receiveErrorsignIn(java.lang.Exception e) {
            }
                


    }
    