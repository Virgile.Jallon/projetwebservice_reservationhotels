package client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;
import org.json.JSONArray;
import org.json.JSONObject;

import client.HotelReservationStub.BookRoom;
import client.HotelReservationStub.CheckHotelsAvailability;
import client.HotelReservationStub.GetConstrainedReservations;
import client.HotelReservationStub.GetReservations;
import client.HotelReservationStub.IsDatabaseReacheable;
import client.HotelReservationStub.SignIn;
import client.HotelReservationStub.SignUp;
import client.HotelReservationStub.UnbookReservation;

/**
 * Classe permettant de lancer le programme.
 * Celui-ci est un programme de type "console"
 */
public class Main {
	
	/**
	 * Le STUB permettant d'accéder aux services SOAP
	 */
	private static HotelReservationStub _stub;
	
	/**
	 * Un JSONObject qui contient des informations sur le client connecté qui utilise le programme
	 */
	private static JSONObject _userData;

	/**
	 * Point d'entrée du programme
	 * Note : Certains comportements anormaux empêchant le bon fonctionnement du programme entraînement
	 * volontairement l'arrêt de celui-ci après avoir affiché un message d'erreur
	 */
	public static void main(String[] args) {
		
		// Instanciation du STUB et vérification de la connexion à la base de données
		try {
			// Instanciation du STUB
			_stub = new HotelReservationStub();
			
			// Vérification de la connexion à la base de données
			IsDatabaseReacheable isDBReacheable = new IsDatabaseReacheable();
			if(! _stub.isDatabaseReacheable(isDBReacheable).get_return()) {
				JOptionPane.showMessageDialog(null, "Impossible de se connecter à la base de données");
				System.exit(1);
			}
		}catch(Exception e) {
			// Une erreur est survenue lors de l'initialisation
			JOptionPane.showMessageDialog(null, "Une erreur est survenue lors de l'initialisation du programme");
			System.exit(2);
		}

		
		// Propose à l'utilisateur de se connecter ou de s'inscrire
		if(choix_connexion_inscription() == 1) {
			// On lance la procédure de connexion
			_userData = connexion("", "");
			if(_userData == null) {
				// La connexion a échouée, on renvoie l'utilisateur sur le menu précédent
				JOptionPane.showMessageDialog(null, "Identifiants incorrects");
				main(null);
			}else {
				try {
					// On souhaite la bienvenue à l'utilisateutr
					String firstName = _userData.getString("first_name");
					String lastName = _userData.getString("last_name");
					System.out.println("Bienvenue "+firstName+" "+lastName);
				}
				catch(Exception e) {
					// Les données de l'utilisateur n'ont pas pu être récupérées
					JOptionPane.showMessageDialog(null, "Erreur lors de la récupération des informations client");
					System.exit(4);
				}	
			}
		}else {
			// On lance la procédure d'inscription
			if(inscription() == false) {
				JOptionPane.showMessageDialog(null, "Erreur lors de la création du compte client");
				System.exit(5);
			}
		}
		
		// Tant que l'utilisateur est connecté, s'il sort d'un menu il est automatiquement redirigé vers le menu principal
		while(_userData != null) {
			naviguerDansMenuPrincipal();
		}
	}
	
	/**
	 * Demande à l'utilisateur de saisir un nombre entier
	 * @param min La valeur minimale que peut prendre le nombre saisi
	 * @param max La valeur maximale que peut prendre le nombre saisi
	 * @param valeursExclues Une liste de valeurs que le nombre saisi ne doit pas prendre
	 * @param previousMessage Le message à afficher avant de demander la saisie
	 * @param errorMessage Le message à afficher si le nombre saisi ne correspond pas à un nombre correct
	 * @return Le 1er nombre saisi qui respecte les règles imposées par les paramètres
	 */
	private static int saisirInt(int min, int max, ArrayList<Integer> valeursExclues, String previousMessage, String errorMessage) {
        int intSaisi = min;
		boolean isCorrect = false;
		// Tant qu'aucun entier saisi ne respecte les règles imposées par les paramètres
        while(isCorrect == false) {
        	
        	// On demande à l'utilisateur de bien vouloir saisir un entier
        	Scanner scanner = new Scanner(System.in);
        	System.out.println(previousMessage);
        	
            try {
            	if(scanner.hasNextInt()) {
                	intSaisi = scanner.nextInt();
                	// On vérifie que l'entier saisi respecte les règles
                	isCorrect = intSaisi >= min && intSaisi <= max && !valeursExclues.contains(intSaisi);
            	}
            } catch (Exception e) {
            	// Si l'entier saisi ne respecte pas les règles, on affiche un message d'erreur
                System.out.println(errorMessage);
                scanner.next();
            }
        }
        
        // On retourne le nombre saisi
        return intSaisi;
	}

	/**
	 * Invite l'utilisateur à naviguer dans le menu principal
	 */
	public static void naviguerDansMenuPrincipal() {
		final String message = "Saisissez le chiffre correspondant à votre choix :"
				+ "\n1 - Voir mes réservations"
				+ "\n2 - Effectuer une réservation"
				+ "\n3 - Déconnexion";
		// Propose à l'utilisateur de chosir parmi les choix proposés
		final int choix = saisirInt(1, 3, new ArrayList<Integer>(), message, "Ce choix est invalide, veuillez effectuer une saisie correcte.");
		
		// Redirige l'utilisateur vers le bon menu selon son choix
		if(choix == 1) {
			consulter();
		}else if(choix == 2){
			reserver();
		}else {
			deconnexion();
		}
	}
	
	/**
	 * Déconnecte l'utilisateur
	 */
	public static void deconnexion() {
		// Déconnexion de l'utilisateur
		_userData = null;
		System.out.println("\nMerci et à très bientôt !");
		
		// On "redémarre" le programme
		main(null);
	}
	
	/**
	 * Permet à l'utilisateur d'effectuer une réservations
	 * Le déroulé de la méthode est le suivant :
	 * - L'utilisateur saisit ses critères de recherche
	 * - Il choisit un hôtel parmi ceux capables de satisfaire sa demande
	 * - Il choisit la/les chambre(s) à réserver
	 * - Si il existe des réservations conflictuelles, on lui propose de les annuler
	 * - On valide sa réservation
	 */
	public static void reserver() {
		
		// L'utilisateur saisit ses critères de recherche
		Scanner scanner = new Scanner(System.in);
		System.out.println("Définissez les paramètres de votre recherche : ");
		String dateArrivee = "";
        boolean isDateFormatOk = false;
        while(isDateFormatOk == false) {
        	System.out.println("Date d'arrivée : (format aaaa-mm-jj)");
    		dateArrivee = scanner.nextLine();
    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setLenient(false);
            try {
                dateFormat.parse(dateArrivee.trim());
                isDateFormatOk = true;
            } catch (Exception e) {
                System.out.println("Le format de la date saisie est incorrect. Veuillez réessayer");
            }
        }
        
        int nbNuits = saisirInt(1, 366, new ArrayList<Integer>(), "Nombre de nuits", "Le nombre de nuits doit être un nombre entier. Veuillez réessayer");
        int nbChambres = saisirInt(1, 10, new ArrayList<Integer>(), "Nombre de chambres", "Le nombre de chambres doit être un nombre entier. Veuillez réessayer");

        
        // On cherche les chambres disponibles
        CheckHotelsAvailability search = new CheckHotelsAvailability();
        search.setArrivalDate(dateArrivee);
        search.setNbNights(String.valueOf(nbNuits));
        search.setNbRooms(String.valueOf(nbChambres));
        try {
			String searchResponse = _stub.checkHotelsAvailability(search).get_return();
			if(searchResponse == null) {
				JOptionPane.showMessageDialog(null, "Erreur lors de la recherche des hôtels/chambres");
			}else if(searchResponse.equals("[]")){
				System.out.println("Aucune chambre ne correspond aux critères recherchés");
			}else {
				JSONArray jsonArray = new JSONArray(searchResponse);
				if(jsonArray.length() == 0) {
					System.out.println("Aucune chambre ne correspond aux critères recherchés");
				} else {
					
					// Recherche des hôtels correspondants à la recherche du client
					ArrayList<Integer> availableHotelsId = getIdsOfAvailableHotels(nbChambres, jsonArray);
					if(availableHotelsId.size() == 0) {
						System.out.println("Aucune chambre ne correspond aux critères recherchés");
					}else {
						
						// Affiche les hotels
						afficherHotels(jsonArray, availableHotelsId.size());
						
						// Demande de choisir un hotel
						int selectedHotelIndex = saisirInt(1, availableHotelsId.size(), new ArrayList<Integer>(), "Saisissez le numéro de l'hôtel désiré", "Votre saisie est incorrecte. Veuillez réessayer");
				      						
						// Sélection des chambres
						ArrayList<Integer> selectedRoomsId = afficherEtSelectionnerChambres(nbChambres, selectedHotelIndex, jsonArray);
						
						// On recherche les réservations conflictuelles
				        GetConstrainedReservations getConstrainedReservations = new GetConstrainedReservations();
				        getConstrainedReservations.setClientId(String.valueOf(_userData.getInt("id")));
				        getConstrainedReservations.setArrivalDate(dateArrivee);
				        getConstrainedReservations.setNbNights(String.valueOf(nbNuits));
				        String getConstrainedReservationsResult = _stub.getConstrainedReservations(getConstrainedReservations).get_return();
				        if(getConstrainedReservationsResult == null) {
				        	// Une erreur est survenue lors de la recherche des réservations conflictuelles
				        	throw new Exception();
				        }
				     
				        if(! getConstrainedReservationsResult.equals("[]")) {
				        	JSONArray contraintes = new JSONArray(getConstrainedReservationsResult);
				        	String message = "ATTENTION : Souhaitez-vous annuler les réservations conflictuelles suivantes : \n";
				        	for(int k = 0; k < contraintes.length(); k++){
							    JSONObject jsonObject = contraintes.getJSONObject(k);
							    message += "Hôtel "+jsonObject.getString("hotelName");
							    message += "\t"+jsonObject.getString("roomName");
							    message += "\t"+jsonObject.getString("price")+"€/nuit";
							    message += "\t"+jsonObject.getString("check_in")+" --> ";
							    message += jsonObject.getString("check_out");
							    message += "\t"+jsonObject.getString("informations")+"\n";
					        }
				        	
				        	message += "\nSaisissez \"oui\" pour annuler ces réservations";
				        	System.out.println(message);
				        	Scanner sc = new Scanner(System.in);
				        	String saisie = sc.nextLine();
				        	if(saisie.toLowerCase().equals("oui")) {
				        		for(int k = 0; k < contraintes.length(); k++){
								    JSONObject jsonObject = contraintes.getJSONObject(k);
								    int id = jsonObject.getInt("id");
								    UnbookReservation unbook = new UnbookReservation();
								    unbook.setReservationID(String.valueOf(id));
								    if(!_stub.unbookReservation(unbook).get_return()) {
								    	System.out.println("ERREUR : La réservation "+jsonObject.getString("hotelName")+" "+jsonObject.getString("roomName")+" n'a pas pu être annulée");
								    }
				        		}
				        	}else {
				        		System.out.println("Vous avez choisi de conserver ces réservations");
				        	}
				        }

			        	// Réservation des chambres
			        	ArrayList<Boolean> bookingStatesList = bookRooms(nbNuits, dateArrivee, selectedRoomsId);
			        	for(int k=0; k<bookingStatesList.size(); k++) {
			        		final String state = bookingStatesList.get(k).equals(true) ? "réussie" : "échouée";
			        		System.out.println("Réservation "+(k+1)+"/"+nbChambres+" : "+state);
			        	}

			        	
			        	
			        	
					}
			        

				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Une erreur est survenue lors de la recherche");
		}
        

	}
	
	
	/**
	 * Réserve des chambres
	 * @param nbNuits Le nombre de nuits que durent les réservations
	 * @param dateArrivee La date de début des réservations
	 * @param roomsId La liste des identifiants des chambres à réserver
	 * @return Un ArrayList<Boolean> qui, pour chaque chambre à réserver associe le statut(réussi/échoué) de la réservation de la chambre.
	 * Null si une exception est levée
	 */
	public static ArrayList<Boolean> bookRooms(int nbNuits, String dateArrivee, ArrayList<Integer> roomsId){
    	ArrayList<Boolean> ret = new ArrayList<Boolean>();
    	try {
    		// Pour chaque chambre, on effectue une réservation
    		for(int id : roomsId) {
    		    BookRoom book = new BookRoom();
    		    book.setClientID(String.valueOf(_userData.getInt("id")));
    		    book.setRoomID(String.valueOf(id));
    		    book.setNbNights(String.valueOf(nbNuits));
    		    book.setArrivalDate(dateArrivee);	
    		    
    		    // On valorise la valeur de retour
    		    ret.add(_stub.bookRoom(book).get_return().equals("true"));
        	}
    		return ret;
    	}
    	catch(Exception e) {
    		return null;
    	}

	}
	
	/**
	 * Affiche la liste des chambres que l'utilisateur peut réserver et l'invite à choisir la/les chambre(s) qu'il désire
	 * @param nbChambres Le nombre de chambres que l'utilisateur veut réserver
	 * @param selectedHotelIndex L'index de l'hôtel propriétaire des chambres
	 * @param roomsData Un JSONArray qui contient de données relatives aux différentes chambres
	 * @return Un ArrayList<Integer> qui contient tous les ID des chambres sélectionnées par l'utilisateur
	 */
	public static ArrayList<Integer> afficherEtSelectionnerChambres(int nbChambres, int selectedHotelIndex, JSONArray roomsData) {
		// Initialisation de variables
		ArrayList<Integer> alreadySelectedIndex = new ArrayList<>();
        ArrayList<Integer> selectedRoomsId = new ArrayList<Integer>();
        ArrayList<Integer> roomsId = new ArrayList<Integer>();
        
        // Pour chaque chambre que l'utilisateur souhaite réserver
        for(int k=0; k<nbChambres; k++) {
        	// Affichage des chambres encore disponibles
        	if(k == 0) {
        		// Si roomsId n'est pas initialisée, l'initialisé
        		roomsId = afficherChambres(selectedHotelIndex, roomsData, alreadySelectedIndex);
        	}else {
		        afficherChambres(selectedHotelIndex, roomsData, alreadySelectedIndex);
        	}

	        if(roomsId != null) {
	        	// On invite l'utilisateur à choisir une chambre parmi celles proposées
	        	int selectedIndex = choisirChambre(k, nbChambres, alreadySelectedIndex, roomsId.size());
	        	alreadySelectedIndex.add(selectedIndex);
	        	selectedRoomsId.add(roomsId.get(selectedIndex-1));
	        }
        }
        
        return selectedRoomsId;
	}
	
	
	/**
	 * Affiche une liste d'hôtels
	 * @param roomsData Un JSONArray contenant des informations relatives aux différentes chambres
	 * @param nbHotelsAAfficher Le nombre d'hôtels maximum à afficher
	 */
	public static void afficherHotels(JSONArray roomsData, int nbHotelsAAfficher) {
		// Initialisation de variables
		ArrayList<String> hotelsWrittenNames = new ArrayList<>();
		int inc = 1;
		try {
			// Parcourt des informations des chambres
			for(int k = 0; k < roomsData.length(); k++){
			    JSONObject jsonObject = roomsData.getJSONObject(k);
			    String name = jsonObject.getString("hotelName");
			    // Si l'hôtel possesseur de la chambre n'a pas encore été affiché
			    if(!hotelsWrittenNames.contains(name)) {
			    	// L'affiche
			    	hotelsWrittenNames.add(name);
			    	System.out.println(inc+" - "+name);
			    	inc ++;
			    }
			    if(hotelsWrittenNames.size() == nbHotelsAAfficher) {
			    	break;
			    }
			}
		}
		catch(Exception e) {
			System.out.println("Une erreur est survenue lors de l'affichage des hôtels");
		}
	}
	
	/**
	 * Récupère les identifiants des hôtels correspondant aux critères de recherche
	 * @param nbChambres Le nombre de chambres à réserver
	 * @param roomsData Un JSONArray contenant des informations relatives à des données sur les chambres
	 * @return Un ArrayList<Integer> contenant la liste des id des hôtels
	 */
	public static ArrayList<Integer> getIdsOfAvailableHotels(int nbChambres, JSONArray roomsData){
		// Initialisation de variables
		ArrayList<Integer> listHotelsId = new ArrayList<>();
		ArrayList<Integer> listNbRooms = new ArrayList<>();
		int hotelId;
		int index;
		
		// Associe à chaque hôtel son nombre de chambres disponibles
		try {
			for(int k = 0; k < roomsData.length(); k++){
			    JSONObject jsonObject = roomsData.getJSONObject(k);
			    hotelId = jsonObject.getInt("hotelId");
			    if(! listHotelsId.contains(hotelId)) {
			    	listHotelsId.add(hotelId);
			    	listNbRooms.add(1);
			    }else {
			    	index = listHotelsId.indexOf(hotelId);
			    	listNbRooms.set(index, listNbRooms.get(index)+1);
			    }
			}	
		}
		catch(Exception e) {
			return new ArrayList<Integer>();
		}
		
		// Récupère les hotels disposants d'assez de chambres
		ArrayList<Integer> availableHotelsId = new ArrayList<>();
		for(int k=0; k<listNbRooms.size(); k++) {
			if(listNbRooms.get(k) >= nbChambres) {
				availableHotelsId.add(listHotelsId.get(k));
			}
		}
		
		return availableHotelsId;
		
	}
	
	/**
	 * Invite l'utilisateur à choisir une chambre parmi celles proposées
	 * @param k Le nombre de chambres déjà sélectionnées +1
	 * @param nbChambres Le nombre de chambres que l'utilisateur veut réserver
	 * @param alreadySelectedIndex Liste des index des chambres déjà sélectionnées
	 * @param maxIndex L'idex max qu'il est possible de saisir
	 * @return L'index de la chambre choisie
	 */
	public static int choisirChambre(int k, int nbChambres, ArrayList<Integer> alreadySelectedIndex, int maxIndex){
		System.out.println("Choisissez votre chambre "+(k+1)+"/"+nbChambres+" : ");
    	int selectedRoomIndex = saisirInt(1, maxIndex, alreadySelectedIndex, "Saisissez le numéro de la chambre désirée", "Votre saisie est incorrecte. Veuillez réessayer");
    	return selectedRoomIndex;
	}
	
	/**
	 * Affiche les chambres disponibles pour une réservation
	 * @param selectedHotelIndex L'index de l'hôtel auquel appartiennent les chambres à afficher
	 * @param roomsData Un JSONArray contenant des informations sur les chambres
	 * @param alreadySelectedIndex La liste des index des chambres déjà sélectionnées
	 * @return ArrayList<Integer> La liste des identifiants de toutes les chambres à afficher. Retourne null si une exception est levée
	 */
	public static ArrayList<Integer> afficherChambres(int selectedHotelIndex, JSONArray roomsData, ArrayList<Integer> alreadySelectedIndex) {
		// Initialisation de variables
		ArrayList<Integer> roomsId = new ArrayList<>(); // Valeur à retourner
        int inc = 1;                                    // Incrément qui sert à associer un numéro à chaque ligne affichée
        String affichageChambre = "";					
        
        try {
        	// On parcourt les chambres
	        for(int k = 0; k < roomsData.length(); k++){
			    JSONObject jsonObject = roomsData.getJSONObject(k);
			    int roomId = jsonObject.getInt("roomId");
			    int hotelId = jsonObject.getInt("hotelId");
			    // Si la chambres appartient bien à l'hôtel concerné
			    if(selectedHotelIndex == hotelId) {
			    	// SI cette chambre n'a pas encore été sélectionnée
			    	if(!alreadySelectedIndex.contains(inc)){
			    		// On l'affiche
				    	affichageChambre = inc+" - "+jsonObject.getString("roomName")+"\t"+jsonObject.getString("price")+"€/nuit\t"+jsonObject.getString("informations");
				    	System.out.println(affichageChambre);
				    	roomsId.add(roomId);
			    	}
			    	inc ++;
			    }
	        } 
	        
	        return roomsId;
		}
		catch (Exception e) {
			return null;
		} 
	}
	
	
	/**
	 * Permet à l'utilisateur de consulter ses réservations déjà effectuées
	 */
	public static void consulter() {
		
		try {
			// Récupération des réservations du client
			GetReservations getReservations = new GetReservations();
			getReservations.setClientId(String.valueOf(_userData.getInt("id")));
			String getReservationsResponse = _stub.getReservations(getReservations).get_return();
			if(getReservationsResponse == null) {
				throw new Exception();
			}else {
				// Affichage des réservations
				JSONArray jsonArray = new JSONArray(getReservationsResponse);
				String s = "";
				if(jsonArray.length() != 0) {
					for(int k = 0; k < jsonArray.length(); k++){
						s = "";
					    JSONObject jsonObject = jsonArray.getJSONObject(k);
					    s += k+1+" - ";
					    s += "Hôtel "+jsonObject.getString("hotelName");
					    s += "\t"+jsonObject.getString("roomName");
					    s += "\t"+jsonObject.getString("price")+"€/nuit";
					    s += "\t"+jsonObject.getString("check_in")+" --> ";
					    s += jsonObject.getString("check_out");
					    s += "\t"+jsonObject.getString("informations");
					    System.out.println(s);
					}
					
					Scanner scanner = new Scanner(System.in);
					String reponse = "";
					do {
						// Offre au client la possibilité d'annuler une réservation ou de retourner au menu pincipal
						System.out.println("Pour annuler une réservation, saisissez \"annuler x\" avec x le numéro de la ligne correspondante");
						System.out.println("Pour retourner au menu précédent, saisisez \" retour\"");
						reponse = scanner.nextLine().toLowerCase();
						if(reponse.equals("retour")) {
							// L'utilisateur est redirigé vers le menu principal
							naviguerDansMenuPrincipal();
						}else if(reponse.startsWith("annuler ")){
							try {
								// Annule la réservation sélectionnée par l'utilisateur
								int index = Integer.parseInt(reponse.split(" ")[1]);
								int reservationId = jsonArray.getJSONObject(index-1).getInt("id");
								UnbookReservation unbook = new UnbookReservation();
								unbook.setReservationID(String.valueOf(reservationId));
								if(_stub.unbookReservation(unbook).get_return()) {
									System.out.println("La réservation "+index+" a bien été annulée.");
								}else {
									JOptionPane.showMessageDialog(null, "Erreur lors de l'annulation d'un réservation");
								}
							}
							catch(Exception e) {
								JOptionPane.showMessageDialog(null, "Erreur lors de l'annulation d'un réservation");
							}
							naviguerDansMenuPrincipal();
						}else {
							System.out.println("Ce choix est invalide, veuillez effectuer une saisie correcte.");
							consulter();
						}
					}
					while(!(reponse.equals("retour") || reponse.startsWith("annuler ")));

					
				}else {
					System.out.println("Vous n'avez encore effectué aucune réservation");
				}
				
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Erreur lors de la récupération des réservations du client");
			naviguerDansMenuPrincipal();
		}
		
	}
	
	
	/**
	 * Demande à l'utilisateur de choisir entre le menu connexion et le menu inscription
	 * @return 1 (Connexion) ou 2 (Inscription)
	 */
	public static int choix_connexion_inscription() {
		final String previousMessage = "Saisissez le chiffre correspondant à votre choix :\n1 - Connexion\n2 - Inscription";
		final String errorMessage = "Ce choix est invalide, veuillez effectuer une saisie correcte.";
		return saisirInt(1, 2, new ArrayList<Integer>(), previousMessage, errorMessage);
	}
	
	
	/**
	 * Connecte l'utilisateur à son compte.
	 * Si celui-ci n'a pas encore saisi ses identifiants, lui demande de saisir son email et son mot de passe pour pouvoir se connecter.
	 * @param email L'email de l'utilisateur
	 * @param mdp Le mot de passe de l'utilisateur
	 * @return Un JSONObject contenant des informations sur l'utilisateur qui vient de se connecter. Null si une exception est survenue
	 */
	public static JSONObject connexion(String email, String mdp) {
		// L'utilisateur n'a pas encore saisi ses identifiants :
		if(email.equals("") && mdp.equals("")) {
			Scanner scanner = new Scanner(System.in);
			// Lui demande de saisir son email
			System.out.println("Saisissez votre adresse e-mail");
			email = scanner.nextLine();
			// Lui demande de saisir son mot de passe
			System.out.println("Saisissez votre mot de passe");
			mdp = scanner.nextLine();	
		}

		try {
			// Initie la connexion de l'utilisateur
			SignIn singIn = new SignIn();
			singIn.setEmail(email);
			singIn.setPassword(mdp);
			String signInResponse = _stub.signIn(singIn).get_return();
			if(signInResponse == null) {
				// Une erreur est survenue lors de la connexion
				return null;
			}
			
			JSONObject jsonObject = new JSONObject(signInResponse);
			return jsonObject;
		}
		catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Dmande à l'utilisateur de saisir des informations afin de pouvoir créer son compte
	 * @return True si l'inscription a pu être effectuée. False sinon
	 */
	public static boolean inscription() {
		// Demande à l'utilisateur de saisir des informations
		Scanner scanner = new Scanner(System.in);
		System.out.println("Saisissez votre prénom");
		String firstName = scanner.nextLine();
		System.out.println("Saisissez votre nom de famille");
		String lastName = scanner.nextLine();
		System.out.println("Saisissez votre adresse e-mail");
		String email = scanner.nextLine();
		System.out.println("Saisissez votre mot de passe");
		String mdp = scanner.nextLine();
		
		try {
			// Initie la création du compte de l'utilisateur
			SignUp signup = new SignUp();
			signup.setFirstName(firstName);
			signup.setLastName(lastName);
			signup.setEmail(email);
			signup.setPassword(mdp);
			
			boolean signUpReturn = _stub.signUp(signup).get_return();
			if(signUpReturn) {
				// Le compte a été créé, on connecte maintenant l'utilisateur à son compte
				_userData = connexion(email, mdp);
				if(_userData != null) {
					System.out.println("Bienvenue "+firstName+" "+lastName);
				}
				// On vérifie que la connexion a correctement fonctionné
				return _userData != null;
			}else {
				return false;
			}
			
		}
		catch (Exception e) {
			return false;
		}
	}

	

}